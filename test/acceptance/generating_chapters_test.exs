defmodule GeneratingChaptersTest do
  use ExUnit.Case
  import Tests.AcceptanceHelpers, only: [step: 1]
  import IO, only: [puts: 1]
  import Enum, only: [map: 2]

  test "Creating a puzzle" do
    chapters = Generation.ChapterGenerator.call()

    map(chapters, fn(chapter) ->
      map(chapter, fn(puzzle) ->
        Util.PuzzlePrinter.call puzzle
      end)
    end)
  end
end
