defmodule CreatingAPuzzleTest do
  use ExUnit.Case
  alias Generation.{PuzzleGenerator}
  alias Model.{Puzzle, Gene, Grid}
  import Tests.AcceptanceHelpers, only: [step: 1]

  test "Creating a puzzle" do
    step "I create a Gene"
    gene = %Gene{seed: "abc", size: 3}

    step "I use the DNA to generate a puzzle"
    puzzle = PuzzleGenerator.call(gene)

    step "I receive a puzzle matching the DNA properties"
    assert Puzzle.size(puzzle) == gene.size

    step "The puzzle has a 2d grid of cells"
    cell = puzzle.grid |> Grid.at(0,0)
    assert cell.number == 1

    step "I see a puzzle"
    Util.PuzzlePrinter.call(puzzle)

    step "I create a bigger puzzle"
    gene = %Gene{seed: "abc", size: 7}
    puzzle = PuzzleGenerator.call(gene)
    Util.PuzzlePrinter.call(puzzle)
  end
end
