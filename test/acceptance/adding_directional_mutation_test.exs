defmodule AddingDirectionalMutationTest do
  use ExUnit.Case
  alias Generation.{PuzzleGenerator}
  alias Mutation.{DirectionalMutator}
  alias Model.{Puzzle, Gene, Grid}
  import Tests.AcceptanceHelpers, only: [step: 1]

  test "Creating a puzzle" do
    step "I create a puzzle with high directional mutation"
    gene = %Gene{seed: "abc", size: 3, mutations: %{ directional: 1.0 } }
    puzzle = PuzzleGenerator.call(gene)
    Util.PuzzlePrinter.call(puzzle)

    step "Some of its normal squares have been replaced with directions"
    non_integer_cells = Enum.reject(Map.values(puzzle.grid), &is_integer/1)
    assert length(non_integer_cells) > 0
  end
end
