defmodule Generation.PuzzleGenerator do
  alias Generation.{PathGenerator, GridGenerator}
  alias Mutation.{DirectionalMutator}
  alias Model.{Gene, Puzzle}

  @doc """
    Given a gene, generates a puzzle.

    # Example
    iex> gene = %Model.Gene{size: 2, seed: "abc"}
    iex> puzzle = Generation.PuzzleGenerator.call(gene)
    iex> Map.size(puzzle.grid)
    4
    iex> Model.Path.is_valid(puzzle.gene.size, puzzle.path)
    true
  """
  def call(gene) do
    path = PathGenerator.call(gene.size, gene.seed)
    grid = GridGenerator.call(path)
    puzzle = %Puzzle{grid: grid, gene: gene, path: path}

    Enum.reduce(all_mutators, puzzle, fn(mutator, accumulator) ->
      mutator.call(gene, accumulator)
    end)
  end

  # Private

  defp all_mutators do
    [DirectionalMutator]
  end
end

