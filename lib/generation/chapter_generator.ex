defmodule Generation.ChapterGenerator do
  alias Model.Gene
  alias Generation.PuzzleGenerator

  def call do
    [
      build_chapter(seed: "01", size: 3, count: 2, mutations: simple_mutations),
      build_chapter(seed: "02", size: 4, count: 3, mutations: simple_mutations),
      build_chapter(seed: "03", size: 4, count: 3, mutations: some_directional_mutations),
      build_chapter(seed: "04", size: 4, count: 3, mutations: all_directional_mutations),
      build_chapter(seed: "05", size: 5, count: 3, mutations: simple_mutations),
      build_chapter(seed: "06", size: 5, count: 5, mutations: some_directional_mutations),
      build_chapter(seed: "07", size: 5, count: 5, mutations: all_directional_mutations),
    ]
  end

  defp build_chapter(seed: seed, size: size, count: count, mutations: mutations) do
    Enum.map(1..count, fn(index) ->
      build_puzzle(seed: seed, size: size, count: count, index: index, mutations: mutations)
    end)
  end

  defp build_puzzle(seed: seed, size: size, count: count, index: index, mutations: mutations) do
    gene = %Gene{seed: "#{seed}#{index}", size: size, mutations: mutations}
    PuzzleGenerator.call(gene)
  end

  defp simple_mutations do
    %{ directional: 0.0 }
  end

  defp some_directional_mutations do
    %{ directional: 0.3 }
  end

  defp all_directional_mutations do
    %{ directional: 1.0 }
  end
end
