defmodule Generation.GridGenerator do
  alias Generation.DistanceCalculator
  import Enum, only: [reduce: 3, zip: 2]
  import Map, only: [put: 3]

  @doc """
    Generates a grid based on a path

    # Examples
    iex> Generation.GridGenerator.call([{0,0}, {0,1}, {1,1}, {1,0}])
    %{
      {0,0} => 1, {0,1} => 1,
      {1,0} => 1, {1,1} => 1
    }
  """
  def call(path) do
    distances = calculate_distances(path)

    zip(path, distances) |> reduce(%{}, fn({ coord, distance }, memo) ->
      put(memo, coord, distance)
    end)
  end

  def calculate_distances([_move]), do: [1]
  def calculate_distances(path) do
    [current | rest] = path
    [next | _others] = rest
    [DistanceCalculator.call(current, next) | calculate_distances(rest)]
  end
end
