defmodule Generation.PathGenerator do
  alias Model.Path

  @doc """
  Given a gene, will generate a path of the gene's size using its seed

  # Examples
  iex> gene = %Model.Gene{size: 1, seed: "aaa"}
  iex> Generation.PathGenerator.call(gene)
  [{0,0}]
  """
  def call(gene) do
    call(gene.size, gene.seed)
  end

  @doc """
  Generates a path through a grid, given a size and random seed.

  # Examples
  iex> Generation.PathGenerator.call(2, "aaa")
  [{1,1}, {1,0}, {0,1}, {0,0}]
  """
  def call(size, seed) do
    chars = seed
      |> String.to_charlist
      |> List.to_tuple
      |> :random.seed

    size |> Path.all_moves |> Enum.sort(fn (_,_) ->
      :random.uniform/1 < 0.5
    end)
  end
end
