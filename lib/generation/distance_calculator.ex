defmodule Generation.DistanceCalculator do
  @doc """
    # Examples
    iex> Generation.DistanceCalculator.call({0,0}, {0,3})
    3
    iex> Generation.DistanceCalculator.call({1,1}, {0,0})
    1
    iex> Generation.DistanceCalculator.call({0,0}, {2,2})
    2
  """
  def call(move1, move2) do
    {x1, y1} = move1
    {x2, y2} = move2

    h_distance = abs(x1 - x2)
    v_distance = abs(y1 - y2)

    max(h_distance, v_distance)
  end
end
