defmodule Model.Gene do
  defstruct \
    seed: nil,
    size: 5,
    mutations: %{ directional: 0.0 }
end
