defmodule Model.Puzzle do
  alias Model.Gene

  defstruct gene: %Gene{}, grid: [], path: []

  def size(puzzle), do: puzzle.gene.size
end
