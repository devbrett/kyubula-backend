defmodule Model.Path do
  import Enum, only: [sort: 1]

  @doc """
    Determines if a series of moves is valid, given a grid size.

    # Examples:
    iex> Model.Path.is_valid(1, [{0,0}])
    true
    iex> Model.Path.is_valid(2, [{0,0}])
    false
  """
  def is_valid(size, moves) do
    all_moves(size) === sort(moves)
  end

  @doc """
  Returns each "skip" from cell to cell with the position and index in pairs, looping on itself.
  iex> Model.Path.in_indexed_pairs([{0,0}, {1,0}])
  [
    [{ {0,0}, 0 }, { {1,0}, 1 }],
    [{ {1,0}, 1 }, { {0,0}, 0 }]
  ]
  """
  def in_indexed_pairs(path) do
    pairs_with_index = path |> Enum.with_index
    pairs_with_index |> Enum.chunk_every(2, 1, pairs_with_index)
  end



  @doc """
    Returns all possible moves for a given grid size.

    # Examples:
    iex> Model.Path.all_moves(2)
    [{0,0}, {0,1}, {1,0}, {1,1}]
  """
  def all_moves(size) do
    for x <- 1..size, y <- 1..size, do: {x-1,y-1}
  end
end
