defmodule Util.PuzzlePrinter do
  # def call(puzzle), do: nil

  def call(puzzle) do
    size = puzzle.gene.size
    seed = puzzle.gene.seed

    IO.puts "{"
    IO.puts "  size: #{size},"
    IO.puts "  seed: #{seed},"
    IO.puts "  cells: ["

    for x <- 0..(size-1) do
      IO.puts "    #{row(puzzle, x)}"
    end

    IO.puts "  ]"
    IO.puts "}"
  end

  def row(puzzle, x) do
    size = puzzle.gene.size

    cells = for y <- 0..(size-1) do
      coord = { x, y }
      distance = puzzle.grid[coord]
      " #{distance},"
    end

    Enum.join(cells)
  end
end
