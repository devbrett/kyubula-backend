defmodule Mutation.DirectionalMutator do
  alias Generation.{PathGenerator}
  alias Model.{Gene, Puzzle, Path}

  @doc """
  Takes a puzzle and its gene, and mutates it by replacing normal cells with directional cells
  iex> gene = %Model.Gene{size: 2, seed: "ccc", mutations: %{ directional: 1.0 } }
  iex> puzzle = Generation.PuzzleGenerator.call gene
  iex> mutated = Mutation.DirectionalMutator.call(gene, puzzle)
  iex> mutated.grid
  %{{0,0} => "D", {0,1} => "L", {1,0} => "R", {1,1} => "U"}
  """
  def call(gene, puzzle) do
    path = gene |> PathGenerator.call
    selected_pairs = path |> Path.in_indexed_pairs |> find_possible_pairs |> select_pairs(gene)
    mutate(puzzle, selected_pairs)
  end

  # Private

  defp mutate(puzzle, selected_pairs) do
    Enum.reduce(selected_pairs, puzzle, fn(pair, puzzle) ->
      [{ position, _index }, _second] = pair
      new_grid = Map.put(puzzle.grid, position, get_direction(pair))
      %{ puzzle | grid: new_grid }
    end)
  end

  defp get_direction([{{fx, fy}, _}, {{sx,sy}, _}]) do
    case {fx > sx, fx < sx, fy > sy, fy < sy} do
      {true, false, false, false} -> "U"
      {false, true, false, false} -> "D"
      {false, false, true, false} -> "L"
      {false, false, false, true} -> "R"
      _anything -> :direction_not_found_error
    end
  end

  defp find_possible_pairs(pairs) do
    pairs |> Enum.filter(fn(pair) ->
      [{first, _}, {second, _}] = pair
      are_positions_inline(first, second)
    end)
  end

  defp are_positions_inline({fx, fy}, {sx, sy}) do
    fx == sx || fy == sy
  end

  defp select_pairs(pairs, gene) do
    chance_for_directional = gene.mutations[:directional]

    pairs |> Enum.filter(fn(pair) ->
      :random.uniform < chance_for_directional
    end)
  end
end
